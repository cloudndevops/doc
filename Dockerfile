### This is for Oralce client installation - version 12.1.0.2
FROM ubuntu:18.04

ARG user=ubuntu
ARG group=ubuntu
ARG uid=1000
ARG gid=1000

ENV DEBIAN_FRONTEND noninteractive
WORKDIR '/app'
#User creation
RUN groupadd -g ${gid} ${group} \
    && useradd -u ${uid} -g ${gid} -m -s /bin/bash ${user}
# Install.
RUN apt-get update -y \
    && apt-get install -y apt-utils \
    && apt-get install -y wget \
    && apt-get install rpm -y \
    && apt-get install vim -y \
    && apt-get install -y alien \
    && apt install unzip
COPY oracle-instantclient12.1*.rpm ./
COPY instantclient-tools-linux.x64-12.2.0.1.0.zip .
RUN alien -i oracle-instantclient12.1*.rpm \
    && apt-get install -y libaio1
RUN unzip instantclient-tools-linux.x64-12.2.0.1.0.zip && rm -rf *zip && cd instant* && cp expdp impdp exp imp /usr/lib/oracle/12.1/client64/bin/
RUN rm -rf *
RUN echo "/usr/lib/oracle/12.1/client64/lib/" > /etc/ld.so.conf.d/oracle.conf && chmod o+r /etc/ld.so.conf.d/oracle.conf \
    && echo "export ORACLE_HOME=/usr/lib/oracle/12.1/client64" > /etc/profile.d/oracle.sh && chmod o+r /etc/profile.d/oracle.sh \
    && echo "export ORACLE_HOME=/usr/lib/oracle/12.1/client64" >> /home/ubuntu/.bashrc \
    && echo "export LD_LIBRARY_PATH=/usr/lib/oracle/12.1/client64/lib/${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}" >> /home/ubuntu/.bashrc \
    && export ORACLE_HOME=/usr/lib/oracle/12.1/client64 && echo "export PATH=$PATH:$ORACLE_HOME/bin" >> /home/ubuntu/.bashrc \
    && ln -s /usr/include/oracle/12.1/client64 $ORACLE_HOME/include
USER ubuntu
CMD ["sqlplus", "-v"]